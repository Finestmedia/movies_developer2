import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class CustomErrorsHandler implements ErrorHandler {
  private showError(error: any) {
    alert('Error occurred. TYPE: ' + error.name + '\n'
      + 'MSG: ' + error.message + '\n'
      + 'ERROR: ' + error.error);
  }

  handleError(error: any) {
    this.showError(error);
    console.error('Error is as follow: ', error);
  }
}
