import {Action} from '@ngrx/store';
import {Movie} from '../../model/movie';

export enum MovieActionTypes {
  LOAD_FILTERED_MOVIES = '[Movie] Load Filtered Movies',
  LOAD_FILTERED_MOVIES_SUCCESS = '[Movie] Load Filtered Movies Success',
  LOAD_FILTERED_MOVIES_FAIL = '[Movie] Load Filtered Movies Fail',

  LOAD_ALL_MOVIES = '[Movie] Load All Movies',
  LOAD_ALL_MOVIES_SUCCESS = '[Movie] Load All Movies Success',
  LOAD_ALL_MOVIES_FAIL = '[Movie] Load All Movies Fail',
}

export class LoadFilteredMovies implements Action {
  readonly type = MovieActionTypes.LOAD_FILTERED_MOVIES;

  constructor(public textSearch: string, public selectedGenres: any[]) {
  }
}

export class LoadFilteredMoviesSuccess implements Action {
  readonly type = MovieActionTypes.LOAD_FILTERED_MOVIES_SUCCESS;

  constructor(public payload: Movie[]) {
  }
}

export class LoadFilteredMoviesFail implements Action {
  readonly type = MovieActionTypes.LOAD_FILTERED_MOVIES_FAIL;

  constructor(public payload: string) {
  }
}

export class LoadAllMovies implements Action {
  readonly type = MovieActionTypes.LOAD_ALL_MOVIES;

}

export class LoadAllMoviesSuccess implements Action {
  readonly type = MovieActionTypes.LOAD_ALL_MOVIES_SUCCESS;

  constructor(public payload: Movie[]) {
  }
}

export class LoadAllMoviesFail implements Action {
  readonly type = MovieActionTypes.LOAD_ALL_MOVIES_FAIL;

  constructor(public payload: string) {
  }
}


export type MovieListAction =
  | LoadFilteredMovies
  | LoadFilteredMoviesSuccess
  | LoadFilteredMoviesFail
  | LoadAllMovies
  | LoadAllMoviesSuccess
  | LoadAllMoviesFail;
