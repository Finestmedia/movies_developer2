import {Action} from '@ngrx/store';
import {Movie} from '../../model/movie';

export enum MovieActionTypes {
  LOAD_MOVIE = '[Movie] Load Movie',
  LOAD_MOVIE_SUCCESS = '[Movie] Load Movie Success',
  LOAD_MOVIE_FAIL = '[Movie] Load Movie Fail',
}

export class LoadMovie implements Action {
  readonly type = MovieActionTypes.LOAD_MOVIE;

  constructor(public payload: number) {
  }
}

export class LoadMovieSuccess implements Action {
  readonly type = MovieActionTypes.LOAD_MOVIE_SUCCESS;

  constructor(public payload: Movie) {
  }
}

export class LoadMovieFail implements Action {
  readonly type = MovieActionTypes.LOAD_MOVIE_FAIL;

  constructor(public payload: string) {
  }
}


export type MovieAction =
  | LoadMovie
  | LoadMovieSuccess
  | LoadMovieFail;
