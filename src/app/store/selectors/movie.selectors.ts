import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromMovie from '../reducers/movie.reducer';

export const getMovieState = createFeatureSelector<fromMovie.MovieState>(
  'movie',
);
export const getMovie = createSelector(getMovieState, fromMovie.getEntity);
