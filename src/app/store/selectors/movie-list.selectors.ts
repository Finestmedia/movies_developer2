import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromMovieList from '../reducers/movie-list.reducer';


export const getMovieListState = createFeatureSelector<fromMovieList.MovieListState>(
  'movies',
);
export const getMovies = createSelector(getMovieListState, fromMovieList.getEntities);
