import { Injectable } from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import { Action } from '@ngrx/store';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {MovieService} from '../../service/movie.service';

import * as movieActions from '../actions/movie.actions';
import {Movie} from '../../model/movie';
import {of} from 'rxjs/internal/observable/of';
import {switchMap} from 'rxjs/internal/operators/switchMap';

@Injectable()
export class MovieEffect {
  constructor(private actions$: Actions, private movieService: MovieService) {}

  @Effect()
  loadMovie$: Observable<Action> = this.actions$.pipe(
    ofType<movieActions.LoadMovie>(
      movieActions.MovieActionTypes.LOAD_MOVIE
    ),
    switchMap((action: movieActions.LoadMovie) =>
      this.movieService.getMovie(action.payload).pipe(
        map(
          (movie: Movie) =>
            new movieActions.LoadMovieSuccess(movie)
        ),
        catchError(err => of(new movieActions.LoadMovieFail(err)))
      )
    )
  );
}
