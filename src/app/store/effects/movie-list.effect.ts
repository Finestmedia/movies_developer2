import { Injectable } from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import { Action } from '@ngrx/store';

import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {of} from 'rxjs/internal/observable/of';
import {switchMap} from 'rxjs/internal/operators/switchMap';

import {MovieService} from '../../service/movie.service';
import * as movieListActions from '../actions/movie-list.actions';
import {Movie} from '../../model/movie';
import {GenreType} from '../../model/movie.model';

@Injectable()
export class MovieListEffect {
  constructor(private actions$: Actions, private movieService: MovieService) {}

  @Effect()
  loadAllMovies$: Observable<Action> = this.actions$.pipe(
    ofType<movieListActions.LoadAllMovies>(
      movieListActions.MovieActionTypes.LOAD_ALL_MOVIES
    ),
    switchMap((action: movieListActions.LoadAllMovies) =>
      this.movieService.getAllMovies().pipe(
        map(
          (movies: Movie[]) =>
            new movieListActions.LoadAllMoviesSuccess(movies)
        ),
        catchError(err => of(new movieListActions.LoadAllMoviesFail(err)))
      )
    )
  );


  @Effect()
  loadFilteredMovies$: Observable<Action> = this.actions$.pipe(
    ofType<movieListActions.LoadFilteredMovies>(
      movieListActions.MovieActionTypes.LOAD_FILTERED_MOVIES
    ),
    switchMap((action: movieListActions.LoadFilteredMovies) =>
      this.movieService.getAllMovies().pipe(
        map((allMovies: Movie[]) => allMovies.filter(movie => {
            return (
              this.isMovieSelected(movie, action)
            );
          })
        )).pipe(
        map(
          (movies: Movie[]) =>
            new movieListActions.LoadFilteredMoviesSuccess(movies)
        ),
        catchError(err => of(new movieListActions.LoadFilteredMoviesFail(err)))
      )
    )
  );

  private isMovieSelected(movie, action: movieListActions.LoadFilteredMovies) {
    return movie.name.toLowerCase().includes(action.textSearch.toLowerCase())
      && this.containGenre(movie.genres, action.selectedGenres);
  }

  private containGenre(movieGenres: GenreType[], selectedGenres: GenreType[]) {
    if (selectedGenres === undefined || selectedGenres.length === 0) {
      return true;
    } else {
      for (const movieGenre of movieGenres) {
        if (selectedGenres.includes(movieGenre)) {
          return true;
        } else {
          return false;
        }
      }
    }
  }
}
