import {Movie} from '../../model/movie';
import * as fromMovies from '../actions/movie-list.actions';

export interface MovieListState {
  entities: Movie[];
  loaded: boolean;
  loading: boolean;
}

export const initialMovieListState: MovieListState = {
  entities: [],
  loaded: false,
  loading: false
};

export function reducer(state = initialMovieListState, action: fromMovies.MovieListAction): MovieListState {

  switch (action.type) {

    // Cases for the filtered movie list
    case fromMovies.MovieActionTypes.LOAD_FILTERED_MOVIES: {
      return {...state, loading: true};
    }
    case fromMovies.MovieActionTypes.LOAD_FILTERED_MOVIES_SUCCESS: {
      return {...state, loading: false, loaded: true, entities: action.payload};
    }
    case fromMovies.MovieActionTypes.LOAD_FILTERED_MOVIES_FAIL: {
      return {...state, loading: false, loaded: false};
    }

    // Cases for the all movies
    case fromMovies.MovieActionTypes.LOAD_ALL_MOVIES: {
      return {...state, loading: true};
    }
    case fromMovies.MovieActionTypes.LOAD_ALL_MOVIES_SUCCESS: {
      return {...state, loading: false, loaded: true, entities: action.payload};
    }
    case fromMovies.MovieActionTypes.LOAD_ALL_MOVIES_FAIL: {
      return {...state, loading: false, loaded: false};
    }
    default:
      return state;
  }
}

export const getEntities = (state: MovieListState) => state.entities;
export const getLoaded = (state: MovieListState) => state.loaded;
export const getLoading = (state: MovieListState) => state.loading;

