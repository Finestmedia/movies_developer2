import {Movie} from '../../model/movie';
import * as fromMovies from '../actions/movie.actions';

export interface MovieState {
  entity: Movie;
  loaded: boolean;
  loading: boolean;
}

export const initialMovieState: MovieState = {
  entity: new Movie(),
  loaded: false,
  loading: false
};

export function reducer(state = initialMovieState, action: fromMovies.MovieAction): MovieState {

  switch (action.type) {
    case fromMovies.MovieActionTypes.LOAD_MOVIE: {
      return {...state, loading: true};
    }
    case fromMovies.MovieActionTypes.LOAD_MOVIE_SUCCESS: {
      return {...state, loading: false, loaded: true, entity: action.payload};
    }
    case fromMovies.MovieActionTypes.LOAD_MOVIE_FAIL: {
      return {...state, loading: false, loaded: false};
    }
    default:
      return state;
  }
}

export const getEntity = (state: MovieState) => state.entity;
export const getLoaded = (state: MovieState) => state.loaded;
export const getLoading = (state: MovieState) => state.loading;
