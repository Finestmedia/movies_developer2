import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import * as fromMovieList from './movie-list.reducer';
import * as fromMovie from './movie.reducer';
import {environment} from '../../../environments/environment';

export interface State {
  movies: fromMovieList.MovieListState;
  movie: fromMovie.MovieState;
}

export const reducers: ActionReducerMap<State> = {
  movies: fromMovieList.reducer,
  movie: fromMovie.reducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
