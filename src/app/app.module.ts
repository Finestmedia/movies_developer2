import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './routing/app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MovieListComponent } from './component/movie-list/movie-list.component';
import { MovieDetailsComponent } from './component/movie-details/movie-details.component';

import { CustomErrorsHandler } from './error/custom-errors-handler';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';

import { MaterialModule } from './material/material';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './store/reducers';
import { EffectsModule, Actions } from '@ngrx/effects';
import { MovieEffect } from './store/effects/movie.effect';
import { MovieListEffect } from './store/effects/movie-list.effect';

@NgModule({
  declarations: [
    AppComponent,
    MovieListComponent,
    MovieDetailsComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    EffectsModule.forRoot([MovieEffect, MovieListEffect]),
    StoreModule.forRoot(reducers, { metaReducers }),
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: CustomErrorsHandler,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
