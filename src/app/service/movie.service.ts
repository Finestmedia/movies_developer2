import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Movie } from '../model/movie';
import { movies } from '../../assets/data/movie.mock-data';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor() {
  }

  getAllMovies(): Observable<Movie[]> {
    return of(movies);
  }

  getMovie(id: number | string): Observable<Movie> {
    return this.getAllMovies()
      .pipe(
        map((allMovies: Movie[]) => allMovies.find(movie => movie.id === +id))
      );
  }
}
