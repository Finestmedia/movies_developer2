import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {Router, ActivatedRoute} from '@angular/router';

import {Movie} from '../../model/movie';
import {MovieService} from '../../service/movie.service';
import * as movieActions from '../../store/actions/movie.actions';
import * as fromMovie from '../../store/reducers/movie.reducer';
import * as movieSelectors from '../../store/selectors/movie.selectors';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  movie$: Observable<Movie>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: MovieService,
    private store: Store<fromMovie.MovieState>
  ) {
    this.movie$ = this.store.select(movieSelectors.getMovie);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = 'id';
      this.store.dispatch(new movieActions.LoadMovie(+params[id]));
    });
  }
}
