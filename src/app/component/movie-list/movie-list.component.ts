import {Component, OnInit} from '@angular/core';
import {Movie} from '../../model/movie';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';

import * as movieListActions from '../../store/actions/movie-list.actions';
import * as fromMovie from '../../store/reducers/movie-list.reducer';
import * as movieListSelector from '../../store/selectors/movie-list.selectors';
import {genreType, GenreType} from '../../model/movie.model';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  movies$: Observable<Movie[]>;

  genresSelector = new FormControl();
  genres: GenreType[] = this.getGenres();

  selectedGenres = [];
  searchText = '';

  constructor(private store: Store<fromMovie.MovieListState>) {
    this.movies$ = this.store.select(movieListSelector.getMovies);
  }

  ngOnInit() {
    this.store.dispatch(new movieListActions.LoadAllMovies());
  }

  applySearch() {
    this.store.dispatch(new movieListActions.LoadFilteredMovies(this.searchText, this.selectedGenres));
  }

  searchByText(searchText: string) {
    this.searchText = searchText;
    this.applySearch();
  }

  searchByGenres(selectedGenres: []) {
    this.selectedGenres = selectedGenres;
    this.applySearch();
  }

  private getGenres() {
    return Object.keys(genreType).map(i => genreType[i]);
  }
}
