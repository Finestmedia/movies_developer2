import {
  trigger,
  animate,
  transition,
  style,
  query
} from '@angular/animations';

export const routeAnimation = trigger('routeAnimation', [
  transition('* <=> *', [
    query(':enter, :leave',
      style({
        position: 'absolute',
        width: '60%',
        opacity: 0,
      }), {optional: true}),

    query(':enter',
      animate('500ms ease',
        style({
          opacity: 1,
        })),
      {optional: true})
  ])
]);
