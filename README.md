# MoviesApplication

Application provides the solution for movies collection browsing. 
Main screen displays the list of the movies where the title is presented.
Clicking on the title expand some basic data like rating and genres.  

There is also possibility to use search engine and provide genre types for filtering
the list. Please, notice- empty filter means that all genres are taken into account.  

User could open the new screen with details for the particular movie by clicking
"Show details" button- this action redirect to the new proper screen with the following
movie's data:
- title
- rating
- length
- genres
- description
- "back to movies list" button

## Build

Run `npm install` to install depend packages.  

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
